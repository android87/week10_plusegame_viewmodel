package com.bobby.week4_plusgame

import android.graphics.Color
import android.os.CountDownTimer
import android.text.format.DateUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import timber.log.Timber

class GameViewModel : ViewModel() {
    private val _typeGame = MutableLiveData<String>()
    val typeGame: LiveData<String>
        get() = _typeGame

    private val _correct   = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct

    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    private val _isSelectAnswer = MutableLiveData<Boolean>()
    val isSelectAnswer: LiveData<Boolean>
        get() = _isSelectAnswer

    private val _randomNum1 = MutableLiveData<String>()
    val randomNum1: LiveData<String>
        get() = _randomNum1

    private val _whichBtnSelect = MutableLiveData<String>()
    val whichBtnSelect: LiveData<String>
        get() = _whichBtnSelect

    private val _randomNum2 = MutableLiveData<String>()
    val randomNum2: LiveData<String>
        get() = _randomNum2

    private val _randomNum3 = MutableLiveData<String>()
    val randomNum3: LiveData<String>
        get() = _randomNum3

    private val _randomBtn = MutableLiveData<String>()
    val randomBtn: LiveData<String>
        get() = _randomBtn

    private val _num1 = MutableLiveData<Int>()
    val num1: LiveData<Int>
        get() = _num1

    private val _num2 = MutableLiveData<Int>()
    val num2: LiveData<Int>
        get() = _num2

    private val _isCorrect = MutableLiveData<String>()
    val isCorrect: LiveData<String>
        get() = _isCorrect

    private val _btnPlayVisible = MutableLiveData<Boolean>()
    val btnPlayVisible: LiveData<Boolean>
        get() = _btnPlayVisible

    private val _btnAnswerColor1 = MutableLiveData<Int>()
    val btnAnswerColor1: LiveData<Int>
        get() = _btnAnswerColor1

    private val _btnAnswerColor2 = MutableLiveData<Int>()
    val btnAnswerColor2: LiveData<Int>
        get() = _btnAnswerColor2

    private val _btnAnswerColor3 = MutableLiveData<Int>()
    val btnAnswerColor3: LiveData<Int>
        get() = _btnAnswerColor3

    private val _currentTime = MutableLiveData<Long>()
    val currentTime: LiveData<Long>
        get() = _currentTime

    val currentTimeString = Transformations.map(currentTime) { time ->
        DateUtils.formatElapsedTime(time)
    }

    private val timer: CountDownTimer

    init {
        Timber.i("GameViewModel created!")
        _typeGame.value = ""
        _correct.value = 0
        _incorrect.value = 0
        _isSelectAnswer.value = false
        _whichBtnSelect.value = ""
        _randomNum1.value = ""
        _randomNum2.value = ""
        _randomNum3.value = ""
        _randomBtn.value = ""
        _num1.value = 0
        _num2.value = 0
        _isCorrect.value = ""
        _btnPlayVisible.value = false
        _btnAnswerColor1.value = Color.LTGRAY
        _btnAnswerColor2.value = Color.LTGRAY
        _btnAnswerColor3.value = Color.LTGRAY
        timer = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {
            override fun onTick(millisUntilFinished: Long) {
                _currentTime.value = millisUntilFinished/ONE_SECOND
            }
            override fun onFinish() {
                _currentTime.value = DONE
                _isSelectAnswer.value = false
                _btnPlayVisible.value = true
                setBtnEndTimer()
                setIncorrect()
            }
        }
    }

    companion object {
        // Time when the game is over
        private const val DONE = 0L
        // Countdown time interval
        private const val ONE_SECOND = 1000L
        // Total time for the game
        private const val COUNTDOWN_TIME = 10000L
    }

    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }

    fun setupUI() {
        _btnPlayVisible.value = false
        _btnAnswerColor1.value = Color.LTGRAY
        _btnAnswerColor2.value = Color.LTGRAY
        _btnAnswerColor3.value = Color.LTGRAY
    }

    fun play(typeGame: String) {
        timer.start()
        _typeGame.value = typeGame
        setupUI()
        randomQuestion(typeGame)
    }

    fun selectAnwserBtn1() {
        timer.cancel()
        _whichBtnSelect.value = "1"
        if (!isSelectAnswer.value!!) {
            if (randomBtn.value == "1") {
                showWin()
            } else {
                showLose()
            }
        }
    }
    fun selectAnwserBtn2() {
        timer.cancel()
        _whichBtnSelect.value = "2"
        if (!isSelectAnswer.value!!) {
            if (randomBtn.value == "2") {
                showWin()
            } else {
                showLose()
            }
        }
    }
    fun selectAnwserBtn3() {
        timer.cancel()
        _whichBtnSelect.value = "3"
        if (!isSelectAnswer.value!!) {
            if (randomBtn.value == "3") {
                showWin()
            } else {
                showLose()
            }
        }
    }

    fun clickPlayAgain() {
        setAnswer()
        play(_typeGame.value!!)
    }

    fun showLose() {
        when (_whichBtnSelect.value) {
            "1" -> {
                _btnAnswerColor1.value = Color.RED
            }
            "2" -> {
                _btnAnswerColor2.value = Color.RED
            }
            "3" -> {
                _btnAnswerColor3.value = Color.RED
            }
        }
        _btnPlayVisible.value = true
        setBtnIncorrectAnswer()
        setIncorrect()

    }

    fun showWin() {
        when (_whichBtnSelect.value) {
            "1" -> {
                _btnAnswerColor1.value = Color.GREEN
            }
            "2" -> {
                _btnAnswerColor2.value = Color.GREEN
            }
            "3" -> {
                _btnAnswerColor3.value = Color.GREEN
            }
        }
        _btnPlayVisible.value = true
        setBtnCorrectAnswer()
        setCorrect()
    }

    fun setBtnEndTimer() {
        _isCorrect.value = "หมดเวลา"
    }

    fun setBtnIncorrectAnswer() {
        _isCorrect.value = "คุณตอบผิด"
    }

    fun setBtnCorrectAnswer() {
        _isCorrect.value = "คุณตอบถูก"
    }

    fun setAnswer() {
        _isSelectAnswer.value = false
    }

    fun setCorrect() {
        _correct.value = _correct.value?.plus(1)
        _isSelectAnswer.value = true
    }

    fun setIncorrect() {
        _incorrect.value = _incorrect.value?.plus(1)
        _isSelectAnswer.value = true
    }

    fun randomQuestion(typeGame: String) {
        _isSelectAnswer.value = false
        _num1.value = (1..10).random()
        _num2.value = (1..10).random()
        _randomBtn.value = (1..3).random().toString()
        when (typeGame) {
            "+" -> {
                _randomNum1.value = ((_num1.value!! + _num2.value!!).toString())
                do {
                    _randomNum2.value = (1..20).random().toString()
                } while (_randomNum2.value == (_num1.value!! + _num2.value!!).toString())
                do {
                    _randomNum3.value = (1..20).random().toString()
                } while (_randomNum3.value == (_num1.value!! + _num2.value!!).toString() || _randomNum3.value == _randomNum2.value)
            }
            "-" -> {
                _randomNum1.value = ((_num1.value!! - _num2.value!!).toString())
                do {
                    _randomNum2.value = (1..10).random().toString()
                } while (_randomNum2.value == (_num1.value!! - _num2.value!!).toString())
                do {
                    _randomNum3.value = (1..10).random().toString()
                } while (_randomNum3.value == (_num1.value!! - _num2.value!!).toString() || _randomNum3.value == _randomNum2.value)
            }
            "x" -> {
                _randomNum1.value = ((_num1.value!! * _num2.value!!).toString())
                do {
                    _randomNum2.value = (1..100).random().toString()
                } while (_randomNum2.value == (_num1.value!! * _num2.value!!).toString())
                do {
                    _randomNum3.value = (1..100).random().toString()
                } while (_randomNum3.value == (_num1.value!! * _num2.value!!).toString() || _randomNum3.value == _randomNum2.value)
            }
        }
        setBtnAnswer()
    }

    fun setBtnAnswer() {
        val temp1 = _randomNum1.value
        val temp2 = _randomNum2.value
        val temp3 = _randomNum3.value
        when (_randomBtn.value) {
            "1" -> {
                _randomNum1.value = temp1
                _randomNum2.value = temp2
                _randomNum3.value = temp3
            }
            "2" -> {
                _randomNum2.value = temp1
                _randomNum1.value = temp2
                _randomNum3.value = temp3
            }
            "3" -> {
                _randomNum3.value = temp1
                _randomNum1.value = temp2
                _randomNum2.value = temp3
            }
        }
    }
}