package com.bobby.week4_plusgame

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import timber.log.Timber

class ScoreViewModel(finalCorrect: Int, finalIncorrect: Int ) : ViewModel() {

    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct

    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    init {
        _correct.value = finalCorrect
        _incorrect.value = finalIncorrect
    }


}